ARG CONTAINER_IMAGE=quay.io/centos/centos
ARG CONTAINER_TAG=stream9

FROM ${CONTAINER_IMAGE}:${CONTAINER_TAG}

RUN dnf install -y \
	dnf-plugin-config-manager \
	epel-release
RUN dnf config-manager -y --set-enabled crb
RUN dnf config-manager --add-repo https://cli.github.com/packages/rpm/gh-cli.repo
RUN dnf -y copr enable @centos-automotive-sig/bluechi-snapshot centos-stream-9 && \
dnf copr enable -y rhcontainerbot/qm centos-stream-9-x86_64


RUN dnf install -y \
	gcc \
	make \
	clang-tools-extra \
	codespell \
	bluechi-ctl \
	bluechi \
	bzip2 \
	cargo \
	rust \
	dbus-devel \
	g++ \
	gh \
	go-toolset \
	fpaste \
	lcov \
	hostname \
	podman \
	python3-podman \
	python3-flake8 \
	python3-pytest \
	python3-pytest-timeout \
	pre-commit \
	pcp \
	sudo \
	gpgme-devel \
	libseccomp-devel \
	iptables-services \
	selinux-policy-targeted \
	selinux-policy-devel \
	ShellCheck \
	systemd \
	valgrind \
	createrepo_c \
	dnf-utils \
	git \
	gzip \
	jq \
	tmt \
	container-selinux \
	kernel \
	kernel-modules \
	passwd \
	python3-gobject \
	python3-pip \
	qm \
	meson \
	npm \
	rpm-build \
	ruby \
	sed \
	vim-enhanced \
	systemd-devel \
	iputils \
	tar \
	golang-github-cpuguy83-md2man \
	telnet \
	net-tools \
	iproute

RUN gem install ruby-dbus
RUN alternatives --install /usr/bin/python python /usr/bin/python3 1
RUN npm install markdownlint-cli2 --global

# Makefile
RUN curl --create-dirs -O --output-dir /root/tests/FFI/ https://raw.githubusercontent.com/containers/qm/main/tests/e2e/tools/FFI/Makefile

# memory - 20_percent_memory_eat
RUN curl --create-dirs -O --output-dir /root/tests/FFI/memory/ASIL/ https://raw.githubusercontent.com/containers/qm/main/tests/e2e/tools/FFI/memory/ASIL/20_percent_memory_eat.c

# memory - 90_percent_memory_eat
RUN curl --create-dirs -O --output-dir /root/tests/FFI/memory/QM/ https://raw.githubusercontent.com/containers/qm/main/tests/e2e/tools/FFI/memory/QM/90_percent_memory_eat.c

# disk - file-allocate.c
RUN mkdir -p /root/tests/FFI/memory/{QM,ASIL}  /root/tests/FFI/disk/{QM,ASIL}
RUN curl --create-dirs -O --output-dir /root/tests/FFI/disk/QM/ https://raw.githubusercontent.com/containers/qm/main/tests/e2e/tools/FFI/disk/QM/file-allocate.c

# bluechi-tester
RUN curl --create-dirs -O --output-dir /root/tests/FFI/bin/ https://raw.githubusercontent.com/containers/bluechi/main/tests/tools/FFI/bluechi-tester
RUN chmod +x /root/tests/FFI/bin/bluechi-tester

# Build all C programs
RUN cd /root/tests/FFI && \
    make all

# setting bluechi and qm
COPY files/ /

RUN curl https://raw.githubusercontent.com/containers/qm/main/setup > /usr/share/qm/setup && \
chmod + /usr/share/qm/setup

RUN /usr/share/qm/setup --skip-systemctl --hostname localrootfs

RUN systemctl enable bluechi-controller && \
systemctl enable bluechi-agent

WORKDIR /root/tests/FFI/bin

CMD /sbin/init
